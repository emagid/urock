<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>




	<!-- HERO SECTION -->
	<section class='hero about_hero' style="  background-image: url('<?php the_field('banner'); ?>');">
		<div class='text_box'>
			<h1><?php the_title(); ?></h1>
		</div>
	</section>

	<section class='statement'>
        <h2>Mission Statement</h2>
		<p><?php the_field('statement'); ?></p>
	</section>

	<!-- HERO SECTION END -->

<section class='bio'>
        <h2><?php the_field('title'); ?></h2>
        <?php the_field('content'); ?>
	</section>



<?php
get_footer();
