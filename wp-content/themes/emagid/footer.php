<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	<!-- FOOTER SECTION ENDS -->
	<footer>
		<a href="/"><p class='logo'>ROLLING ROCK VENTURES</p></a>
		<div class='footer_holder'>
			<div class="links">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1'
                    ) );
                ?>
			</div>
			<div class="news">
				<p class='industry'>INDUSTRY NEWS</p>
                
                            <?php
	  			$args = array(
	    		'post_type' => 'industry_news'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
                <a href="<?php the_field('link'); ?>" target="_blank">
                <p><?php the_field('short_intro'); ?></p>
                </a>
                
                
            <?php
                }
                    }
                else {
                echo 'No Projects Found';
                }
            ?> 
			</div>
            
			<div class="updates">
				<div class='left_center'>
					<p>Sign Up for Updates</p>
					<div class="form-group" id="signup">
						<input type="text" class="input" name="email" id="email" placeholder="Email" style="width: 200px;">
						<button class='button' style="margin: 0 auto;"> SUBMIT </button>
					</div>
					<div class="social-icons">
						<a href="#" class="facebook"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb.png"></a>
						<a href="#" class="instagram"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ig.png"></a>
						<a href="#" class="twitter"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png"></a>
						<a href="https://www.linkedin.com/company/rrventures" class="linkedin"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/linkedin_1.png"></a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- FOOTER SECTION ENDS -->
	

<?php wp_footer(); ?>

</body>
</html>
