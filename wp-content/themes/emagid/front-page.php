<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero'>
		<div class='text_box'>
			<h1><?php the_field('hero_title'); ?></h1>
			<p>See if you have what it takes to roll with us.</p>
			<a href='/contact' class='button'> LETS TALK </a>
		</div>
		<div class="circle_wrapper">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hero_piechart.png" height="225">
			<div class="mask_left"></div>
			<div class="mask_right"></div>
			<div></div>			
		</div>
	</section>

	<!-- HERO SECTION END -->



	<!-- LOGO SECTION -->
	<section class='client_logos'>
		<h2><?php the_field('portfolio_section_header'); ?></h2>
		<div>
<!-- 			<div class='logos'>
				<a href="portfolioPage.html">
					<div class='overlay'><p>VIEW NOW</p></div>
					<img class="client_logo grey" src="<?php echo get_template_directory_uri(); ?>/assets/img/certify_color.png">
				</a>
			</div> -->
			<p><?php the_field('portfolio_section_text'); ?></p>
		</div>
		<a class='view_more' href="/portfolio"><button class='button'>VIEW ALL COMPANIES</button></a>
	</section>
	<!-- LOGO SECTION END -->



	<!-- ABOUT US SECTION -->
	<section class='about_us'>
		<div class='about_text'>
			<h2><?php the_field('about_section_header'); ?></h2>
			<div>
				<p><?php the_field('about_section_text'); ?></p>
                <a href="/about">
				    <button class='button'>LEARN MORE</button>
                </a>
			</div>
		</div>
		<div class='float_right_img' style="  background-image: url(<?php the_field('team_background'); ?>);"><a class='button inverse' href="/team">VIEW TEAM</a></div>

		<!-- 	
		<div class="services">
			<div class='service'>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service1.png">
				<h4>Service 1</h4>
	  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			</div>
			<div class='service'>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service2.png">
				<h4>Service 2</h4>
	  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			</div>
			<div class='service'>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service3.png">
				<h4>Service 3</h4>
	  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			</div>
		</div> -->
	</section>
	<!-- ABOUT US END  -->




	<!-- GROWTH -->
	<section class='growth'>
		<div>
			<h2><?php the_field('growth_section_header'); ?></h2>
			<p><?php the_field('growth_section_text'); ?></p>
		</div>

		<div>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/rocket-launch.png" width="60%">
<!--			<h4>STEP ONE</h4>-->
			<p><?php the_field('step_one'); ?></p>
		</div>
	</section>
    <section class="talk_button">
        <div>
        <a href="/contact">
           <button class='lets_talk button'> LETS TALK </button>
        </a>
            </div>
    </section>
	<!-- GROWTH -->



<?php
get_footer();
